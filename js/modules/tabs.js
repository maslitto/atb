var tabs = (function () {
	return {
		init: function () {
			tabs.tabsSwitchAction();
            var md = new MobileDetect(window.navigator.userAgent);
            if(md.phone()){
                $('.js-tabs-nav').slick({
                    slidesToShow: 2,
                    //centerMode: true,
                    dots: false,
                    arrows: false,
                    variableWidth: true
                });
			}
		},
		/*tabsSwitchAction: function() {

			$('.js-tabs').each(function() {
				var $tabsWrap = $(this),
					$tabLinks = $tabsWrap.find('.js-tabs__link');

				$tabLinks.click(function(e){
					e.preventDefault();
					var $currentTabLink   = $(this);
					tabs.changeTab($currentTabLink);
				});
			});
		},*/
        tabsSwitchAction: function() {
			$('.js-tabs').each(function() {
                var $tabsWrap = $(this),
                    $tabLinks = $tabsWrap.find('.js-tabs__link');
                $tabLinks.each(function (index, value) {
                    $(value).attr('data-index',index);
                    console.log($(value).data('index'));
                });
                $tabLinks.click(function(e){
                    e.preventDefault();
                	var $currentTabLink   = $(this);
                    tabs.changeTab($currentTabLink);
                });
            });

        },

		/*changeTab: function($tabLinkToSwitch) {
			var $tabsWrap    = $tabLinkToSwitch.closest('.js-tabs'),
				$tabLinks    = $tabsWrap.find('.js-tabs__link'),
				$tabs 	     = $tabsWrap.find('.js-tabs__tab');

			$tabLinks.removeClass('active');
			$tabLinkToSwitch.addClass('active');
			$tabs.removeClass('active');
			$tabs.eq($tabLinkToSwitch.index()).addClass('active');
		},*/
        changeTab: function($tabLinkToSwitch) {
            var
                $tabsWrap    = $tabLinkToSwitch.closest('.js-tabs'),
                $tabLinks    = $tabsWrap.find('.js-tabs__link'),
                $tabs 	     = $tabsWrap.find('.js-tabs__tab')
                //, $index       = $tabLinkToSwitch.closest('.slick-active').data('slick-index')
            ;
            $tabLinks.removeClass('active');
            $tabLinkToSwitch.addClass('active');
            $tabs.removeClass('active');
            $tabs.eq($tabLinkToSwitch.data('index')).addClass('active');
        }
	}
})();