"use strict";

//= vendor/jquery.min.js
//= vendor/inputmask/inputmask.js

//= ../node_modules/magnific-popup/dist/jquery.magnific-popup.js
//= ../node_modules/mobile-detect/mobile-detect.js
//= ../node_modules/slick-carousel/slick/slick.js
//= ../node_modules/sticky-kit/dist/sticky-kit.js
//= ../node_modules/select2/dist/js/select2.js
//= ../node_modules/floatthead/dist/jquery.floatThead.js

//= modules/tabs.js
//= modules/forms.js


var app = (function () {
	return {
		init: function () {
		    //modules
		    tabs.init();
            forms.init();

            //app methods
            app.popups();
            app.initCarousel();
            app.initGallery();
            app.questionary();
            app.map();
            app.mobileMenu();
            app.stickyHeader();
            app.select2();
            app.initPriceCarousel();
            app.initSpamProtection();
		},
        initSpamProtection: function () {
		    $(document).ready(function () {
                $('.js-antispam').val('no-robots-please');
            });

        },
        initPriceCarousel: function () {
            var md = new MobileDetect(window.navigator.userAgent);
            if(md.phone()){
                $('.js-carousel').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    cssEase: 'linear',
                    arrows: false,
                    centerMode: true,
                    variableWidth: true
                });
            }

        },

        select2: function () {
            $('.js-select').select2({
                minimumResultsForSearch: -1
            });
            $('.js-select').on('select2:select', function (e) {
                var data = e.params.data;
                console.log(data.id);
                $('.js-mobile-table').find('tr td').hide();
                $('.js-mobile-table').find('tr td:nth-child('+data.id+')').css('display','table-cell');
                $('.js-mobile-table').find('tr td:first-of-type').css('display','table-cell');
            });
        },

        stickyHeader: function() {

            $('.introduction .table').floatThead({
                zIndex: 2
            });
            var nav = $('.header');
            var lastScrollTop;

            window.addEventListener("scroll", function() {  // listen for the scroll
                var st = window.pageYOffset || document.documentElement.scrollTop;
                if (st > lastScrollTop){
                    nav.css('opacity','0'); // hide the nav-bar when going down
                    nav.removeClass('sticky');
                }

                else {
                    nav.css('opacity', 1); // display the nav-bar when going up
                    nav.addClass('sticky');
                }
                lastScrollTop = st;
                if(st < 10){
                    nav.removeClass('sticky');
                }
                //nav.style.top = '${st}px'; // set the position of the nav-bar to the current scroll
            }, false);
        },

        initCarousel: function () {
            $('.js-slick').slick({
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 6000,
                fade: true,
                cssEase: 'linear',
                arrows: false
            });
        },

        initGallery: function () {
            $('.js-certs').magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Загрузка #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function(item) {
                        return item.el.attr('title') + '<small>Сертификаты</small>';
                    }
                }
            });
        },

        popups: function () {

            $('.js-popup').click(function (e) {
                e.preventDefault();
                var src = $(this).attr('href');

                if($(this).hasClass('product')){
                    var title = $(this).find('.product__title').text();
                    $(src).find('.js-popup__header').text('Купить '+ title);
                    $(src).find('.js-service').val(title);
                }
                if(!(typeof ($(this).data('title'))=== undefined)){
                    console.log(typeof ($(this).data('title')));
                    $(src).find('.js-popup__header').text($(this).data('title'));
                    $(src).find('.js-service').val($(this).data('title'));
                }
                if(!(typeof ($(this).data('title'))=== undefined)){
                    $(src).find('.js-popup__description').text($(this).data('description'));
                }
                $.magnificPopup.open({
                    items: {
                        src: src, // can be a HTML string, jQuery object, or CSS selector
                        type: 'inline'
                    }
                })
            });

            $('.js-mfp-close').click(function () {
                var magnificPopup = $.magnificPopup.instance;
                magnificPopup.close();
            });

            $('.js-popup-back').click(function (e) {
                e.preventDefault();
                var $form = $(this).closest('.js-form'),
                    $success = $form.find('.js-form__success'),
                    $content = $form.find('.js-form__content');
                $success.hide();
                $content.show();
            });

            $('.js-popup__form').on('form_success', function() {
                $(this).find('.js-form__content').hide();
                $(this).find('.js-form__success').show();
                $('.js-form-success-phone').text($(this).find('.js-phone').val());
            });

        },

        map: function () {
		    if(typeof(ymaps) !== 'undefined'){
                ymaps.ready(function () {
                    var md = new MobileDetect(window.navigator.userAgent);
                    if(md.phone()){
                        var center = [59.932036, 30.420522];
                    } else {
                        var center = [59.931036, 30.415432];
                    }
                    var myMap = new ymaps.Map('map', {
                        center: center,
                        zoom: 16,
                        controls: []
                    }, {
                        searchControlProvider: 'yandex#search'
                    });
                    var marker = new ymaps.Placemark([59.932036, 30.420522], {
                        hintContent: '195112, г. Санкт-Петербург, Заневский пр-т, д. 30, к. 2, литера А, помещение 5-Н, оф. 314',
                        balloonContent: '195112, г. Санкт-Петербург, Заневский пр-т, д. 30, к. 2, литера А, помещение 5-Н, оф. 314'
                    }, {
                        iconLayout: 'default#image',
                        iconImageHref: 'img/map-marker.png',
                        iconImageSize: [41, 48],
                        iconImageOffset: [-5, -38]
                    });

                    myMap.behaviors.disable('scrollZoom');
                    myMap.geoObjects.add(marker)

                });
            }

        },

        questionary: function () {

            $('.js-next-step').click(function (e) {
                e.preventDefault();
                var $questionary = $(this).closest('.js-questionary'),
                    id = parseInt($questionary.attr('data-step'))
                ;
                if(id == 1){
                    $questionary.toggleClass('small');
                }
                $('.js-step').removeClass('active');
                var $next = $questionary.find('[data-id="'+String(id + 1)+'"]');
                $next.addClass('active');
                if(id == 5){
                    $('.js-form-row').hide();
                }
                $questionary.attr('data-step',id + 1);
            });

            $('.js-prev-step').click(function (e) {
               e.preventDefault();
                var $questionary = $(this).closest('.js-questionary'),
                    id = parseInt($questionary.attr('data-step'))
                ;
                if(id == 2){
                    $questionary.removeClass('small');
                }
                $('.js-step').removeClass('active');
                var $next = $questionary.find('[data-id="'+String(id - 1)+'"]');
                $next.addClass('active');
                $questionary.attr('data-step',id - 1);
            });

            $('.js-questionary-form').on('form_success', function() {
                $('.js-step').removeClass('active');
                $(this).find('.js-form__success').show();
                $('.js-form-success-phone').text($(this).find('.js-phone').val());
            });
        },

        mobileMenu: function () {
            $('.js-menu-toggler').click(function (e) {
                e.preventDefault();
                $('.js-mobile-menu').toggleClass('open');
            })
        }
	}

})();

$(document).on('ready', app.init);






