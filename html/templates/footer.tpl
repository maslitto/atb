<footer class="footer">
	<div class="footer__arch">
		<img src="img/arch.svg" alt="" width="100%">
	</div>
	<div class="container">
		<div class="row footer__row">
			<div class="footer__logo-block">
				<div class="footer__logos-wrapper">
					<img src="img/logo.png" alt="" class="footer__logo"><img src="img/bmg.png" alt="" class="footer__bsmg">
				</div>
				<p class="footer__copyright">© Бизнес Менеджмент Групп - Автоматизация 2019</p>
			</div>
			<div class="footer__socials">
				<a href="#" class="footer__social--vk footer__social" rel="nofollow" target="_blank"></a>
				<a href="#" class="footer__social--facebook footer__social" rel="nofollow" target="_blank"></a>
				<a href="#" class="footer__social--insta footer__social" rel="nofollow" target="_blank"></a>
			</div>
			<div class="footer__contacts">
				<div class="footer__phone">
					+7 812 244-13-52
				</div>
				<div class="footer__icons">
					<a href="#callback" class="footer__icon icon-phone icon-phone--black js-popup"></a><a href="#mail" class="footer__icon icon-mail icon-mail--black js-popup"></a>
				</div>
			</div>

		</div>
	</div>
</footer>

<div class="popup mfp-hide" id="consult">
	<form action="#" class="form js-form popup__form js-popup__form" method="POST">
		<input type="hidden" class="js-atispam" name="antispam" value="">
		<div class="form__content js-form__content">
			<div class="popup__header js-popup__header">
				Бесплатная консультация
			</div>
			<div class="popup__description js-popup__description">
				Обсудите вашу задачу с ведущим специалистом<br>
				по автоматизации бизнес-процессов.
			</div>
			<div class="form__group">
				<input type="text" class="form__input-text js-input js-required" placeholder="Имя">
			</div>
			<div class="form__group">
				<input type="tel" class="form__input-text js-input js-phone js-required" placeholder="Телефон">
			</div>
			<div class="form__group">
				<button class="btn" type="submit">Получить консультацию</button>
			</div>
			<div class="popup__info">
				<div class="popup__percent">%</div>
				<div class="popup__consultation"><strong>Бесплатная</strong> консультация и <strong>скидка 20%</strong><br>
					на обследование или выполнение первой<br>
					задачи по 1С</div>
			</div>
		</div>
		<div class="form__success js-form__success">
			<div class="popup__header js-popup__header">
				Спасибо!
			</div>
			<div class="popup__success-text">
				
				Ваша заявка <strong><span class="js-success-number"></span></strong> принята.
				В самое ближайшее время мы вам позвоним
				на номер <span class="js-form-success-phone">+7 123 345-67-89</span>
			</div>
			<div class="popup__back-wrapper"><a href="#" class="back js-popup-back">← Исправить данные</a></div>
		</div>
	</form>
</div>
<div class="popup mfp-hide" id="buy">
	<form action="#" class="form js-form popup__form js-popup__form" method="POST">
		<input type="hidden" class="js-antispam" name="antispam" value="">
		<input type="hidden" class="js-service" name="service" value="">

		<div class="form__content js-form__content">
			<div class="popup__header js-popup__header">
				Купить продукт
			</div>
			<div class="popup__description js-popup__description">
				Оставьте заявку на покупку.
			</div>
			<div class="form__group">
				<input type="text" class="form__input-text js-input js-required" placeholder="Имя">
			</div>
			<div class="form__group">
				<input type="tel" class="form__input-text js-input js-phone js-required" placeholder="Телефон">
			</div>
			<div class="form__group">
				<button class="btn" type="submit">Отправить</button>
			</div>
			<div class="space--100"></div>
		</div>
		<div class="form__success js-form__success">
			<div class="popup__header js-popup__header">
				Спасибо!
			</div>
			<div class="popup__success-text">
				Ваша заявка <strong><span class="js-success-number"></span></strong> принята.
				В самое ближайшее время мы вам позвоним
				на номер <span class="js-form-success-phone">+7 123 345-67-89</span>
			</div>
			<div class="popup__back-wrapper"><a href="#" class="back js-popup-back">← Исправить данные</a></div>
		</div>
	</form>
</div>
<div class="popup mfp-hide popup--message" id="mail">
	<form action="#" class="form js-form popup__form js-popup__form" method="POST">
		<input type="hidden" class="js-atispam" name="antispam" value="">
		<div class="form__content js-form__content">
			<div class="popup__header js-popup__header">
				Напишите нам
			</div>
			<div class="popup__description js-popup__description">
				По любому вопросу, предложению или жалобе.
			</div>
			<div class="form__group">
				<input type="text" class="form__input-text js-input js-required" placeholder="Имя">
			</div>
			<div class="form__group">
				<input type="tel" class="form__input-text js-input js-phone js-required" placeholder="Телефон">
			</div>
			<div class="form__group">
				<textarea class="form__textarea js-input js-required" placeholder="Комментарий" rows="6"></textarea>
			</div>
			<div class="form__group">
				<button class="btn" type="submit">Отправить</button>
			</div>

		</div>
		<div class="form__success js-form__success">
			<div class="popup__header js-popup__header">
				Спасибо!
			</div>
			<div class="popup__success-text">
				Ваша заявка <strong><span class="js-success-number"></span></strong> принята.
				В самое ближайшее время мы вам позвоним
				на номер <span class="js-form-success-phone">+7 123 345-67-89</span>
			</div>
			<div class="popup__back-wrapper"><a href="#" class="back js-popup-back">← Исправить данные</a></div>
		</div>
	</form>
</div>
<div class="popup mfp-hide" id="callback">
	<form action="#" class="form js-form popup__form js-popup__form" method="POST">
		<input type="hidden" class="js-atispam" name="antispam" value="">
		<input type="hidden" class="js-service" name="service" value="">

		<div class="form__content js-form__content">
			<div class="popup__header js-popup__header">
				Закажите обратный звонок
			</div>
			<div class="popup__description js-popup__description">
				Наш специалист перезвонит вам
			</div>
			<div class="form__group">
				<input type="text" class="form__input-text js-input js-required" placeholder="Имя">
			</div>
			<div class="form__group">
				<input type="tel" class="form__input-text js-input js-phone js-required" placeholder="Телефон">
			</div>
			<div class="form__group">
				<input type="text" class="form__input-text js-input" placeholder="Удобное время">
			</div>
			<div class="form__group">
				<button class="btn" type="submit">Заказать звонок</button>
			</div>
			<div class="space--100"></div>
		</div>
		<div class="form__success js-form__success">
			<div class="popup__header js-popup__header">
				Спасибо!
			</div>
			<div class="popup__success-text">
				Ваша заявка <strong><span class="js-success-number"></span></strong> принята.
				В самое ближайшее время мы вам позвоним
				на номер <span class="js-form-success-phone">+7 123 345-67-89</span>
			</div>
			<div class="popup__back-wrapper"><a href="#" class="back js-popup-back">← Исправить данные</a></div>
		</div>
	</form>
</div>
<div class="popup mfp-hide popup--message" id="boss">
	<form action="#" class="form js-form popup__form js-popup__form" method="POST">
		<input type="hidden" class="js-atispam" name="antispam" value="">
		<div class="form__content js-form__content">
			<div class="popup__header js-popup__header">
				Обратная связь
			</div>
			<div class="popup__description js-popup__description">
				Напишите генеральному директору нашей компании
			</div>
			<div class="form__group">
				<input type="text" class="form__input-text js-input js-required" placeholder="Имя">
			</div>
			<div class="form__group">
				<input type="tel" class="form__input-text js-input js-phone js-required" placeholder="Телефон">
			</div>
			<div class="form__group">
				<textarea class="form__textarea js-input js-required" placeholder="Комментарий" rows="6"></textarea>
			</div>
			<div class="form__group">
				<button class="btn" type="submit">Отправить</button>
			</div>

		</div>
		<div class="form__success js-form__success">
			<div class="popup__header js-popup__header">
				Спасибо!
			</div>
			<div class="popup__success-text">
				Ваша заявка <strong><span class="js-success-number"></span></strong> принята.
				В самое ближайшее время мы вам позвоним
				на номер <span class="js-form-success-phone">+7 123 345-67-89</span>
			</div>
			<div class="popup__back-wrapper"><a href="#" class="back js-popup-back">← Исправить данные</a></div>
		</div>
	</form>
</div>
<script src="https://api-maps.yandex.ru/2.1/?apikey=cf0a961d-cf10-432a-97ee-645e98c7f520&lang=ru_RU" type="text/javascript"></script>
<script src="js/app.js"></script>

</body>
</html>