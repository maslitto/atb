<!doctype html>

<html lang="ru">
<head>
	<meta charset="utf-8">

	<title>АТБ Консалт</title>
	<meta name="description" content="">
	<meta name="namewords" content="">
	<meta name="author" content="">
	<link href="img/logo.png" data-rjs="2" rel="icon" type="image/png">
	<link rel="stylesheet" href="css/app.css?v=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>

<body>

<header class="header js-header" id="header">
	<div class="container">
		<div class="header__logo-wrapper">
			<a href="/" class="header__logo"><img src="img/logo.png" alt=""></a>
		</div>
		<ul class="header__menu menu hidden-mobile">
			<li class="menu__item">Компания
				<ul class="submenu">
					<li class="submenu__item"><a href="about.html" class="menu__link">О компании</a></li>
					<li class="submenu__item"><a href="certs.html" class="menu__link">Сертификаты</a></li>
					<li class="submenu__item"><a href="projects.html" class="menu__link">Проекты</a></li>
					<li class="submenu__item"><a href="contacts.html" class="menu__link">Контакты</a></li>
				</ul>
			</li>
			<li class="menu__item">Услуги
				<ul class="submenu">
					<li class="submenu__item"><a href="sell1c.html" class="menu__link">Продажа программ 1с</a></li>
					<li class="submenu__item"><a href="configure.html" class="menu__link">Настройка и доработка 1с</a></li>
					<li class="submenu__item"><a href="introduction.html" class="menu__link">Комплексное внедрение 1с</a></li>
					<li class="submenu__item"><a href="support.html" class="menu__link">Сопровождение 1с, ИТС</a></li>
				</ul>
			</li>
		</ul>
		<div class="mobile-menu js-mobile-menu">
			<div class="mobile-menu__header">
				<img src="img/logo.png" alt="" class="mobile-menu__logo">
				<div class="header__icons">
					<a href="#" class="header__icon icon-phone icon-phone--white"></a><a href="#" class="header__icon icon-mail icon-mail--white"></a><a
							href="#" class="header__icon icon-close js-menu-toggler visible-mobile"></a>
				</div>
			</div>
			<ul class="mobile-menu__menu">
				<li class="mobile-menu__item"><a href="sell1c.html" class="mobile-menu__link">Продажа программ 1с</a></li>
				<li class="mobile-menu__item"><a href="configure.html" class="mobile-menu__link">Настройка и доработка 1с</a></li>
				<li class="mobile-menu__item"><a href="introduction.html" class="mobile-menu__link">Комплексное внедрение 1с</a></li>
				<li class="mobile-menu__item"><a href="support.html" class="mobile-menu__link">Сопровождение 1с, ИТС</a></li>
				<li class="mobile-menu__item"><a href="about.html" class="mobile-menu__link">О компании</a></li>
				<li class="mobile-menu__item"><a href="certs.html" class="mobile-menu__link">Сертификаты</a></li>
				<li class="mobile-menu__item"><a href="projects.html" class="mobile-menu__link">Проекты</a></li>
				<li class="mobile-menu__item"><a href="contacts.html" class="mobile-menu__link">Контакты</a></li>
			</ul>
			<div class="mobile-menu__footer">
				<div class="footer__socials">
					<a href="#" class="footer__social--vk footer__social"></a>
					<a href="#" class="footer__social--facebook footer__social"></a>
					<a href="#" class="footer__social--insta footer__social"></a>
				</div>
			</div>
		</div>
		<div class="header__phone hidden-mobile">+7 812 244-13-52</div>
		<div class="header__icons">
			<a href="#callback" class="header__icon icon-phone icon-phone--white js-popup"></a><a href="#mail" class="header__icon icon-mail icon-mail--white js-popup"></a><a
					href="#" class="header__icon icon-menu js-menu-toggler visible-mobile"></a>
		</div>
	</div>

</header>
